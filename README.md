## Running locally

1. Clone the [mfe-poc](https://gitlab.com/sam.ouimette/mfepoc)
2. Switch to the `single-spa` branch
3. Follow the repo's README to run it locally
4. Clone this repo
5. Install dependencies running the command `yarn`
6. Run the command `yarn start`
7. Open `http://localhost:9000/` on your browser